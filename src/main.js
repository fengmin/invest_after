// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router';
import store from './store'
// import network from '../static/js/network'
import apiInterface from '../static/js/interface'
import COMMON from '../static/js/common'

Vue.config.productionTip = false


// Vue.use(network);
Vue.use(apiInterface);
Vue.use(COMMON);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})

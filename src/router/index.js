import Vue from 'vue'
import Router from 'vue-router'
import List from '@/view/ProjectList'
import SearchList from '@/view/SearchList'
import Detail from '@/view/Details'
import Summary from '@/view/Summary'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/list',
      // name: 'list',
      component: List
    },
    {
      path: '/searchList',
      // name: 'list',
      component: SearchList
    },
    {
      path: '/detail',
      // name: 'details',
      component: Detail
    },
    {
      path: '/summary',
      // name: 'summary',
      component: Summary
    },
    {
      path: '/',
      component: List
    }
  ]
})

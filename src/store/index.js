/**
 * Created by fengmin on 2017/11/6.
 */
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex)

const state = {
  commonModalFlag:true,
  commonModalOption:{
    type:'loading',
    title:'',
    content:'',
    confirmWord:'确认',
    cancelWord:'取消'
  },
  admin_token: '',
  user:{},
  listDatas:[]
}

const mutations = {
  changeCommonModalFlag(state,val){
    console.log('changeCommonModalFlag===',val);
    state.commonModalFlag = val
  },
  changeCommonModalOption(state,obj){
    Object.assign(state.commonModalOption,obj);
  },
  changeAdminToken(state,admin_token){
    state.admin_token = admin_token;
  },
  changeUser(state,user){
    Object.assign(state.user,user);
  },
  changeListDatas(state,obj){
    Object.assign(state.listDatas,obj);
  }
}
const actions = {
  login(context, obj){
    return new Promise((resolve, reject) => {
      let str_token = document.URL.split("?token=")[1];
      let url = SERVER.api.login, param = {};
      if (document.URL.indexOf("localhost") !== -1 || document.URL.indexOf("10.1.25.87") !== -1) {
        url = SERVER.api.login_fz;
        param = {
          workcode: "068108"
        }
      } else {
        url = SERVER.api.login;
        param = {
          token: str_token
        }
      }
      axios({
        method: 'post',
        url: url,
        params:param,
        responseType: 'json',
        headers: {'Authorization': context.state.admin_token||'no admin_token'}
      }).then((response) => {
        let data = response.data.data
        context.commit('changeAdminToken',data.token)
        context.commit('changeUser',data.user)
        context.commit('changeCommonModalFlag',false)

        resolve(response);
      }).catch((response) => {
        if(response=='Error: Network Error'){
          context.commit('changeCommonModalFlag',true)
          context.commit('changeCommonModalOption', {content: '网络错误',type:'alert'})
          return;
        }else{
          reject(response);
        }
      });
    })
  },
  getCategories(context,obj){
    return new Promise((resolve,reject) => {
      let url = SERVER.api.getCategories;
      axios({
        method:'get',
        url:url,
        responseType: 'json',
        headers: {'Authorization': 'Bearer ' + (context.state.admin_token||'no admin_token')}
      }).then((response) => {
        let data = response.data.data
        resolve(data);
      }).catch((response) => {
        if(response=='Error: Network Error'){
          context.commit('changeCommonModalFlag',true)
          context.commit('changeCommonModalOption', {content: '网络错误',type:'alert'})
          return;
        }else{
          reject(response);
        }
      });
    })

  },
  getListDatas(context, obj){
    return new Promise((resolve, reject) => {
      let url = SERVER.api.getProjectList, param = {};
      axios({
        method: 'get',
        url: url,
        params:param,
        responseType: 'json',
        headers: {'Authorization': 'Bearer ' + (context.state.admin_token||'no admin_token')}
      }).then((response) => {
        let data = response.data.data
        resolve(data);
        context.commit('changeListDatas',data)
      }).catch((response) => {
        if(response=='Error: Network Error'){
          context.commit('changeCommonModalFlag',true)
          context.commit('changeCommonModalOption', {content: '网络错误',type:'alert'})
          return;
        }else{
          reject(response);
        }
      });
    })
  },
  sortListDatas(context, obj){
    return new Promise((resolve, reject) => {
      let url = SERVER.api.getProjectList, param = {};
      Object.assign(param,obj);
      axios({
        method: 'get',
        url: url,
        params:param,
        responseType: 'json',
        headers: {'Authorization': 'Bearer ' + (context.state.admin_token||'no admin_token')}
      }).then((response) => {
        let data = response.data.data
        resolve(data);
        context.commit('changeListDatas',data)
      }).catch((response) => {
        if(response=='Error: Network Error'){
          context.commit('changeCommonModalFlag',true)
          context.commit('changeCommonModalOption', {content: '网络错误',type:'alert'})
          return;
        }else{
          reject(response);
        }
      });
    })
  },
  searchListDatas(context,obj){
    let keyword = obj.keyword;
    return new Promise((resolve, reject) => {
      let url = SERVER.api.searchProjectList, param = {keyword};
      axios({
        method: 'get',
        url: url,
        params:param,
        responseType: 'json',
        headers: {'Authorization': 'Bearer ' + (context.state.admin_token||'no admin_token')}
      }).then((response) => {
        let data = response.data.data
        resolve(data);
        context.commit('changeListDatas',data)
      }).catch((response) => {
        if(response=='Error: Network Error'){
          context.commit('changeCommonModalFlag',true)
          context.commit('changeCommonModalOption', {content: '网络错误',type:'alert'})
          return;
        }else{
          reject(response);
        }
      });
    })
  },
  getDetail(context, obj){
    return new Promise((resolve, reject) => {
      let url = SERVER.api.projectDetail;
      axios({
        method: 'get',
        url: `${url}/${obj.id}`,
        // params:param,
        responseType: 'json',
        headers: {'Authorization': 'Bearer ' + (context.state.admin_token||'no admin_token')}
      }).then((response) => {
        let data = response.data.data
        resolve(data);
      }).catch((response) => {
        if(response=='Error: Network Error'){
          context.commit('changeCommonModalFlag',true)
          context.commit('changeCommonModalOption', {content: '网络错误',type:'alert'})
        }else{
          reject(response);
        }

      });
    })
  },
  getFollow(context, obj){
    return new Promise((resolve, reject) => {
      let url = SERVER.api.projectFollow;
      axios({
        method: 'get',
        url: `${url}/${obj.id}`,
        // params:param,
        responseType: 'json',
        headers: {'Authorization': 'Bearer ' + (context.state.admin_token||'no admin_token')}
      }).then((response) => {
        let data = response.data.data
        resolve(data);
      }).catch((response) => {
        if(response=='Error: Network Error'){
          context.commit('changeCommonModalFlag',true)
          context.commit('changeCommonModalOption', {content: '网络错误',type:'alert'})
          return;
        }else{
          reject(response);
        }
      });
    })
  }
}

export default new Vuex.Store({
  state,
  actions,
  mutations,
})

(function () {
  if (!window.COMMON) {
    window.COMMON = {
      floatPow:100000,
      formatDate:function (date, fmt) {
        date = new Date(date)
        var o = {
          "M+": date.getMonth() + 1,                 //月份
          "d+": date.getDate(),                    //日
          "h+": date.getHours(),                   //小时
          "m+": date.getMinutes(),                 //分
          "s+": date.getSeconds(),                 //秒
          "q+": Math.floor((date.getMonth() + 3) / 3), //季度
          "S": date.getMilliseconds()             //毫秒
        }

        if (/(y+)/.test(fmt))
          fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o)
          if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
      },
      getRegs:function(){
        return {
          // reg : /^[0-9]+(.[0-9]{2})?$/,
          regOne : /^[0-9]+(.[0-9]{1})?$/,
          regE : /^(0|[1-9][0-9]*)$/,
          // reg:/^(0|[1-9]\d*)(\.\d{1,2})?$/匹配非负数及一到两位小数
          reg:/^((\d+(\.\d+)?)|(0+(\.0+)?))$/
        }
      },
      getYear:function(){
        var thedate = new Date();
        return thedate.getFullYear();
      },
      getQuarter:function(){
        var date = new Date();
        var month = date.getMonth() + 1;
        var quarter = 1
        if (month <4) {
          quarter = 1;
        } else if (month < 7) {
          quarter = 2;
        } else if (month < 10) {
          quarter = 3;
        } else {
          quarter = 4;
        }
        return quarter;
      }
    };
  }
})(window);



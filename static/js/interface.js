(function () {
  var HOST={
    environment :'develop',
    host:{
      develop:'http://invest-api.info.iii-space.com/',
      production:"http://invest-api.info.100tal.com/",
    }
  };
  window.SERVER={
    api:{
      login: HOST.host[HOST.environment] + 'api/login',
      login_fz: HOST.host[HOST.environment] + 'api/login_fz',
      getProjectList: HOST.host[HOST.environment] + 'api/projects',
      searchProjectList: HOST.host[HOST.environment] + 'api/project/search',
      projectDetail: HOST.host[HOST.environment] + 'api/project',
      projectFollow: HOST.host[HOST.environment] + 'api/follow',
      getCategories: HOST.host[HOST.environment] + 'api/classes'
    }
  };
})();

